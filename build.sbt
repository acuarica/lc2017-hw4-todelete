
// sbt.version = 0.13.13

lazy val root = (project in file("."))
  .settings(
  name := "LC2017-HW4",
  scalaVersion := "2.12.1"
)

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"
libraryDependencies += "com.github.nikita-volkov" % "sext" % "0.2.4"
