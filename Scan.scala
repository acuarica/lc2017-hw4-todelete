object Scan {
  trait Token
  case object AND extends Token
  case object OR  extends Token
  case object NOT extends Token
  case object TRUE extends Token
  case object FALSE extends Token
  case object LEFT extends Token
  case object RIGHT extends Token
  case class ID(x: String) extends Token

  class Scanner(input: String) {
    var chars = input.toList

    // Return the next token, or None if illegal or end of input
    def nextToken: Option[Token] = chars.toList match {
      case ch::tail if ch.isWhitespace => 
        chars = tail
        nextToken
      case '&'::'&'::tail => 
        chars = tail
        Some(AND)
      case '|'::'|'::tail => 
        chars = tail
        Some(OR)
      case '!'::tail => 
        chars = tail
        Some(NOT)
      case '('::tail => 
        chars = tail
        Some(LEFT)
      case ')'::tail => 
        chars = tail
        Some(RIGHT)
      case c::tail if Character.isJavaIdentifierStart(c) =>
        val suffix = tail.takeWhile(Character.isJavaIdentifierPart)
        chars = tail.drop(suffix.length)
        val x = (c::suffix).mkString
        x match {
          case "true" => Some(TRUE)
          case "false" => Some(FALSE)
          case x => Some(ID(x))
        }
      case c::tail => 
        println(s"unexpected character '$c'")
        println(input)
        println(" " * (input.length - tail.length - 1) + "^")
        None
      case Nil =>
        // end of input
        None
    }
  }

  // def main(args: Array[String]) = {
  //   val input = "true && xyzzy123 && ! trueStartsAnIdentifier||(false&&true    ) || ! false"
  //   println(input)
  //   val s = new Scanner(input)
  //   var t = s.nextToken
  //   while (t != None) {
  //     println(t.get)
  //     t = s.nextToken
  //   }
  // }

}
