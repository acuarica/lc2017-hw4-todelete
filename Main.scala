
object Main {

  def main(args: Array[String]) = {
    for (ln <- io.Source.stdin.getLines) {
      println(s"> $ln")
      println(Parse.parse(ln) != None)
    }
  }

}
