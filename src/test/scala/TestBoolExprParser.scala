
import org.scalatest.FunSuite
import org.scalatest.BeforeAndAfter

import Parse._
import Expr._

class ParserTests extends FunSuite with BeforeAndAfter {

  testExpr(
    "((!((!!true))) && false) || ((false && !true))",
    Or(And(Not(Not(Not(True))), False), And(False, Not(True)))
  )

  testExpr("true", True)
  testExpr("false", False)
  testExpr("!true", Not(True))
  testExpr("!false", Not(False))
  testExpr("!!true", Not(Not(True)))
  testExpr("!!!false", Not(Not(Not(False))))
  testExpr("true || false", Or(True, False))
  testExpr("true && false", And(True, False))
  testExpr("(true)", True)
  testExpr("true || false || true", Or(True, Or(False, True)))
  testExpr("true && false && true", And(True, And(False, True)))
  testExpr("true && false || true", And(True, Or(False, True)))
  testExprFail("true ||")
  testExprFail("true &&")
  testExprFail("true !")
  testExprFail("!")
  testExprFail("!!")
  testExprFail("!!!")
  testExprFail("&&")
  testExprFail("||")

  def testExpr(s: String, expected: Expr) = test(s) {
    Parse.parse(s) match {
      case Some(actual) => assertResult(expected) { actual }
      case None => fail()
    }
  }

  def testExprFail(s: String) = test(s) {
    Parse.parse(s) match {
      case Some(actual) => fail(actual.toString)
      case None => 
    }
  }
}
