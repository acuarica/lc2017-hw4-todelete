
import Scan._

// LL(1) grammar
// S  ::= E
// E  ::= T | T || E
// T  ::= F | F && E
// F  ::= ! S | true | false | ( E )

trait Expr
case class Or(left: Expr, right: Expr) extends Expr
case class And(left: Expr, right: Expr) extends Expr
case class Not(expr: Expr) extends Expr
case class Val(v: Boolean) extends Expr

object Expr {
  val True = Val(true)
  val False = Val(false)
}

object Parse {

  def parse(input: String): Option[Expr] = {
    val p = new Parser(input)
    p.parseS
  }

  class Parser(input: String) {

    val s = new Scanner(input)
    var t: Option[Token] = None

    private def next = {
      t = s.nextToken
    }

    def parseS: Option[Expr] = {
      next
      val r = parseE
      if (t == None) r else None
    }

    def parseE: Option[Expr] = {
      parseT match {
        case Some(left) => t match {
          case Some(OR) => next; parseE match {
            case Some(right) => Some(Or(left, right))
            case _ => None
          }
          case None => Some(left)
          case Some(_) => Some(left)
        }
        case _ => None
      }
    }

    def parseT: Option[Expr] = {
      parseF match {
        case Some(left) => t match {
          case Some(AND) => next; parseE match {
            case Some(right) => Some(And(left, right))
            case _ => None
          }
          case None => Some(left)
          case Some(_) => Some(left)
        }
        case _ => None
      }
    }

    def parseF: Option[Expr] = {
      t match {
        case Some(TRUE)=> next; Some(Val(true))
        case Some(FALSE) => next; Some(Val(false))
        case Some(LEFT) => next; parseE match {
          case Some(e) => if (t == Some(RIGHT)) { next; Some(e) } else None
          case _ => None
        }
        case Some(NOT) => next; parseE match {
          case Some(e) => Some(Not(e))
          case _ => None
        }
        case _ => None
      }
    }
  }

}
